#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h>
#include <termios.h>
#include <signal.h>

#define STDIN 0

int mid;

struct termios default_termios;

void set_default_termios(){
  tcgetattr(STDIN, &default_termios);
}

void canonical_mode(bool canonical){
  if(canonical){
    // Resetting termios
    tcsetattr(STDIN, TCSANOW, &default_termios);
    return;
  }
  struct termios info;
  tcgetattr(STDIN, &info);
    
  info.c_lflag &= ~ICANON;
  info.c_cc[VMIN] = 1;
  info.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &info);
}

void clear() {
  const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J\0";
  write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}


struct czlonek{
    char imie[32];
    char nazwisko[32];
};

struct lista{
    int lacznie;
    struct czlonek dane[32];
};

struct msgbuf{
    long type;
	int ID;
	int numerPolecenia;
	char napis1[32];
	char napis2[64];
	char napis3[2048];
    struct lista osoby;
}my_msg;

struct mojeDane{
    int ID;
    char imie[32];
    char nazwisko[32];
}mojeDane;

struct wiadomosc{
    char imie[32];
    char nazwisko[64];
    char tresc[2048];
};

struct wiadomosci{
    int lacznie;
    struct wiadomosc wiadomosc[32];
};

void await(){
  canonical_mode(false);
  getchar();
  getchar();
  canonical_mode(true);
}


void zapiszDoPliku(){
    FILE *grupa;
    char pom[68];
    strcpy(pom, "../data/");
    strcpy(pom,mojeDane.imie);
    strcat(pom,mojeDane.nazwisko);
    strcat(pom, ".dat");
    
    grupa = fopen(pom, "r");
    if (grupa == NULL){
      printf("Error opening group file\n");
      return;
    }
    int rozmiar;
    struct wiadomosci wiadomosci;
    fscanf(grupa,"%d",&rozmiar);
    wiadomosci.lacznie=rozmiar+1;
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%s %s %s", wiadomosci.wiadomosc[i].imie, wiadomosci.wiadomosc[i].nazwisko, wiadomosci.wiadomosc[i].tresc);
    }
    fclose(grupa);

    FILE *blokowani1;
    blokowani1 = fopen(pom,"w");
    fprintf(blokowani1,"%d\n",wiadomosci.lacznie);
    for(int i=0;i<rozmiar;i++){
        fprintf(blokowani1,"%s %s %s\n", wiadomosci.wiadomosc[i].imie,
         wiadomosci.wiadomosc[i].nazwisko, wiadomosci.wiadomosc[i].tresc);
    }
    fprintf(blokowani1,"%s %s %s", my_msg.napis1,
        my_msg.napis2, my_msg.napis3);
    fclose(blokowani1);
}

void odbierzOdpowiedz(){
  do{
    msgrcv(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),mojeDane.ID,0);
    if(my_msg.numerPolecenia==10){
      zapiszDoPliku();
    }
    else{
      break;
    }
  }
  while(true);
}

int logIn(){
  my_msg.type = 1;
  my_msg.ID = getpid();
  my_msg.numerPolecenia = 1;
  printf("Wprowadz login: ");
  scanf("%s", my_msg.napis1);
  printf("Wprowadz haslo: ");
  scanf("%s",my_msg.napis2);
  
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  //printf("type: %d my_msg.ID: %d my_msg.numerPolecenia: %d Login: %s Haslo: %s\n",my_msg.type, my_msg.ID, my_msg.numerPolecenia, my_msg.napis1, my_msg.napis2);
  msgrcv(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),getpid(),0);
  if(my_msg.ID==getpid()) {
    printf("%s\n", my_msg.napis3);
    if(my_msg.numerPolecenia == 3) exit(0);
    return 0;
  }
  mojeDane.ID = my_msg.ID;
  strcpy(mojeDane.imie,my_msg.napis1);
  strcpy(mojeDane.nazwisko,my_msg.napis2);
  printf("Witaj %s %s\n",mojeDane.imie,mojeDane.nazwisko);
  return 1;
}

void logOut(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 2;
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n", my_msg.napis3);
}

void sigint_handler(){
  clear();
  logOut();
  exit(0);
}

void listOnlineUsers(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 3;
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  for(int i=0;i<my_msg.osoby.lacznie;i++){
    printf("%s %s\n", my_msg.osoby.dane[i].imie,my_msg.osoby.dane[i].nazwisko);
  }
}

void listGroupMembers(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 4;
  printf("Podaj nazwe grupy tematycznej ktorej czlonkow chcesz zobaczyc: ");
  scanf("%s",my_msg.napis1);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  if(my_msg.numerPolecenia==0){
    printf("%s\n", my_msg.napis3);
    return;
  }
  printf("Oto lista osob nalezacych do grupy %s:\n",my_msg.napis1);
  for(int i=0;i<my_msg.osoby.lacznie;i++){
    printf("%s %s\n", my_msg.osoby.dane[i].imie,my_msg.osoby.dane[i].nazwisko);
  }
  return;
}

void joinGroup(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 5;
  printf("Podaj nazwe grupy tematycznej do jakiej chcesz dolaczyc: ");
  scanf("%s",my_msg.napis1);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;    
}

void leaveGroup(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 6;
  printf("Podaj nazwe grupy tematycznej z jakiej chcesz odejsc: ");
  scanf("%s",my_msg.napis1);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;
}

void listGroups(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 7;
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("Oto lista dostepnych grup tematycznych:\n");
  for(int i=0;i<my_msg.osoby.lacznie;i++){
    printf("%s\n",my_msg.osoby.dane[i].imie);
  }
  return;
}

void messageGroup(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 8;
  printf("Podaj nazwe grupy do ktorej chcesz wyslac wiadomosc: ");
  scanf("%s",my_msg.napis1);
  printf("Podaj tresc wiadomosci ktora chcesz wyslac: ");
  scanf("%s",my_msg.napis3);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;    
}

void messageUser(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 9;
  printf("Podaj imie uzytkownika do ktorego chcesz wyslac wiadomosc: ");
  scanf("%s",my_msg.napis1);
  printf("Podaj nazwisko uzytkownika do ktorego chcesz wyslac wiadomosc: ");
  scanf("%s",my_msg.napis2);
  printf("Podaj tresc wiadomosci ktora chcesz wyslac: ");
  scanf("%s",my_msg.napis3);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;    
}

void receiveMessage(){
  FILE *blokowani;
  char imieINazwisko[64];
  strcpy(imieINazwisko,"./data/");
  strcat(imieINazwisko,mojeDane.imie);
  strcat(imieINazwisko,mojeDane.nazwisko);
  strcat(imieINazwisko,".dat");
  blokowani = fopen(imieINazwisko,"r");
  int rozmiar;
  fscanf(blokowani,"%d",&rozmiar);
  char nazwiskoNadawcy[64];
  char imieNadawcy[32];
  char wiadomosc[2048];
  for(int i=0;i<rozmiar;i++){
    fscanf(blokowani,"%s %s %s", imieNadawcy, nazwiskoNadawcy,wiadomosc);
    printf("%s %s %s\n", imieNadawcy, nazwiskoNadawcy,wiadomosc);
  }
  fclose(blokowani);
  
  FILE *pusty;
  pusty = fopen(imieINazwisko,"w");
  fprintf(pusty,"%d", 0);
  fclose(pusty);
  
  my_msg.type = mojeDane.ID;
  my_msg.numerPolecenia = 0;
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
}

void blockUser(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 11;
  printf("Podaj imie uzytkownika ktorego chcesz zablokowac: ");
  scanf("%s",my_msg.napis1);
  printf("Podaj nazwisko uzytkownika ktorego chcesz zablokowac: ");
  scanf("%s",my_msg.napis2);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;    
}

void blockGroup(){
  my_msg.type = 1;
  my_msg.numerPolecenia = 12;
  printf("Nazwe grupy ktora chcesz zablokowac: ");
  scanf("%s",my_msg.napis1);
  msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
  odbierzOdpowiedz(mid);
  printf("%s\n",my_msg.napis3);
  return;    
}

int main(){
  set_default_termios();
  signal(SIGINT, sigint_handler);
  mid = msgget(0x123456, 0644|IPC_CREAT);
  bool logged_in = false;
  while(!logged_in){
    clear();
    logged_in = (bool)(logIn(mid));
    await();
  }
  
  while(true){
    clear();
    int nrpolecenia;
    if (1){
      printf("1. Wyloguj sie\n");
      printf("2. Wyswietl zalogowanych uzytkownikow\n");
      printf("3. Wyswietl czlonkow grupy\n");
      printf("4. Dolacz do grupy\n");
      printf("5. Wyjdz z grupy\n");
      printf("6. Wyswietl liste grup\n");
      printf("7. Wyslij wiadomosc do grupy\n");
      printf("8. Wyslij wiadomosc do uzytkownika\n");
      printf("9. Odbierz wiadomosc\n");
      printf("10. Zablokuj uzytkownika\n");
      printf("11. Zablokuj grupe\n");
      
      printf("Wprowadz numer polecenia: ");
    }
    else {
      printf("1. Log out\n");
      printf("2. List online users\n");
      printf("3. List group members\n");
      printf("4. Join a group\n");
      printf("5. Leave a group\n");
      printf("6. List groups\n");
      printf("7. Send a message to a group\n");
      printf("8. Send a message to a user\n");
      printf("9. Receive a message\n");
      printf("10. Block a user\n");
      printf("11. Block a group\n");
      printf("Enter operation number: ");
    }
    scanf("%d", &nrpolecenia);
    clear();
    fflush(stdout);
    void (*commands[11])(void) = {
      logOut, listOnlineUsers,
      listGroupMembers, joinGroup,
      leaveGroup, listGroups, messageGroup,
      messageUser, receiveMessage, blockUser,
      blockGroup
    };
    if (nrpolecenia+1 < 2 || nrpolecenia+1 > 12){
      printf("Wybrano niedopuszczalny numer\n");
      await();
      fflush(stdout);
      continue;
    }
    commands[nrpolecenia-1]();
    if (nrpolecenia-1 == 0) exit(0);
    await();
    fflush(stdout);
  }
  
  return 0;
}
