#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
//odbiera wiadomosci
// użyj w konsoli "ipcs" by zobaczyć kolejki komunikatów
// "ipcs -q" pokazuje tylko kolejke kolunikatow
// "ipcs -q id" usuwa kolejkę komunikatów
struct czlonek{
    char imie[32];
    char nazwisko[32];
};

struct lista{
    int lacznie;
    struct czlonek dane[32];
};

struct msgbuf{
    long type;
	int ID;
	int numerPolecenia;
	char napis1[32];
	char napis2[64];
	char napis3[2048];
    struct lista osoby;
}my_msg;

struct uzytkownikLogowania{
    int PID;
    int podejscie;
};

struct bazaLogowania{
    int lacznie;
    struct uzytkownikLogowania logowanie[32];
}logowania;

struct uzytkownik{
    int ID;
    char imie[32];
    char nazwisko[32];
};

struct bazaZalogowanych{
    int lacznie;
    struct uzytkownik uzytkownicy[32];
}zalogowani;

struct pomoc{
    int lacznie;
    struct uzytkownik uzytkownicy[32];
};

struct IDID{
    int IDBlokujacego;
    int IDZablokowanego;
};

struct zablokowani{
    int lacznie;
    struct IDID blokowani[64];
};

void wyswietlBazeLogowania(){
    printf("Bledne logowania:\n");
    for(int i=0;i<logowania.lacznie;i++){
        printf("PID: %d Podejscie: %d\n",logowania.logowanie[i].PID,logowania.logowanie[i].podejscie);
    }
    return;
}

void wyswietlBazeZalogowanych(){
    printf("Zalogowani:\n");
    for(int i=0;i<zalogowani.lacznie;i++){
        printf("ID: %d Imie: %s Nazwisko %s\n",zalogowani.uzytkownicy[i].ID,zalogowani.uzytkownicy[i].imie, zalogowani.uzytkownicy[i].nazwisko);
    }
    return;
}

int ktoraProbaLogowania(){
    for(int i=0;i<logowania.lacznie;i++){
        if(logowania.logowanie[i].PID == my_msg.ID){
            logowania.logowanie[i].podejscie++;
            return logowania.logowanie[i].podejscie;
        }
    }
    logowania.logowanie[logowania.lacznie].PID = my_msg.ID;
    logowania.logowanie[logowania.lacznie].podejscie=1;
    logowania.lacznie++;
    return 1;
}

int passwordCompare(){
    FILE *hasla;
    hasla = fopen("./data/passwords.dat","r");
    if (hasla == NULL){
      printf("Error opening passwords file\n");
      return 0;
    }
    
    int rozmiar;
    fscanf(hasla,"%d",&rozmiar);
    int ID;
    char login[32], haslo[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(hasla,"%d %s %s",&ID, login, haslo);
        if(strncmp(login,my_msg.napis1,32)==0&&strncmp(haslo,my_msg.napis2,32)==0) return ID;
    }
    return 0;
}

void usuniecieZBazyBlednychLogowan(){
    int i=0;
        for(;i<logowania.lacznie;i++){
            if(logowania.logowanie[i].PID == my_msg.ID){
                logowania.logowanie[i].podejscie=0;
                logowania.logowanie[i].PID =0;
                break;
            }
            
        }
        i++;
        for(;i<logowania.lacznie;i++){
            logowania.logowanie[i-1].podejscie=logowania.logowanie[i].podejscie;
            logowania.logowanie[i-1].PID = logowania.logowanie[i].PID;
        }
    logowania.lacznie--;
}

void polecenie1(){
  int ID = passwordCompare();
  if(ID==0){
    my_msg.type = my_msg.ID;
    if(ktoraProbaLogowania() == 3){
      my_msg.numerPolecenia = 3;
      strcpy(my_msg.napis3,"Zbyt duza liczba nieudanych prob logowania, nastepuje wylaczenie");
      usuniecieZBazyBlednychLogowan();
    }
    else{
      strcpy(my_msg.napis3,"Bledny login lub haslo");
    }
    
    return;
  }
  
    FILE *uzytkownicy;
    uzytkownicy = fopen("./data/users.dat","r");
    int rozmiar;
    fscanf(uzytkownicy,"%d",&rozmiar);
    int IDu;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(uzytkownicy,"%d %s %s",&IDu, imie, nazwisko);
        //printf("ID: %d imie: %s nazwisko %s\n",ID, imie, nazwisko);
        if(IDu==ID){
            usuniecieZBazyBlednychLogowan();

            my_msg.type = my_msg.ID;
            my_msg.ID = ID;
            strcpy(my_msg.napis1,imie);
            strcpy(my_msg.napis2,nazwisko);

            zalogowani.uzytkownicy[zalogowani.lacznie].ID = ID;
            strcpy(zalogowani.uzytkownicy[zalogowani.lacznie].imie, imie);
            strcpy(zalogowani.uzytkownicy[zalogowani.lacznie].nazwisko, nazwisko);
            zalogowani.lacznie++;

            return;
        }
    }
    printf("Blad, istnieje haslo a nie ma uzytkownika");
    return;
}

void usuniecieZBazyZalogowanych(){
    int i=0;
        for(;i<zalogowani.lacznie;i++){
            if(zalogowani.uzytkownicy[i].ID == my_msg.ID){
                zalogowani.uzytkownicy[i].ID = 0;
                strcpy(zalogowani.uzytkownicy[i].imie,"");
                strcpy(zalogowani.uzytkownicy[i].nazwisko,"");
                break;
            }
            
        }
        i++;
        for(;i<zalogowani.lacznie;i++){
            zalogowani.uzytkownicy[i-1].ID = zalogowani.uzytkownicy[i].ID;
            strcpy(zalogowani.uzytkownicy[i-1].imie,zalogowani.uzytkownicy[i].imie);
            strcpy(zalogowani.uzytkownicy[i-1].nazwisko,zalogowani.uzytkownicy[i].nazwisko);
        }
    zalogowani.lacznie--;
}

void polecenie2(){
    my_msg.type = my_msg.ID;
    usuniecieZBazyZalogowanych();
    strcpy(my_msg.napis3, "Pomyslnie wylogowano, zyczymy milego dnia :)");
}

void polecenie3(){
    my_msg.type = my_msg.ID;
    my_msg.osoby.lacznie = zalogowani.lacznie;
    for(int i=0;i<zalogowani.lacznie;i++){
        strcpy(my_msg.osoby.dane[i].imie,zalogowani.uzytkownicy[i].imie);
        strcpy(my_msg.osoby.dane[i].nazwisko,zalogowani.uzytkownicy[i].nazwisko);
    }
    strcpy(my_msg.napis3,"Oto aktualna lista zalogowanych uzytkonikow:");
    return;
}

int czyIstniejeDanaGrupa(){
    FILE *grupy;
    grupy = fopen("./data/grupyTematyczne.dat","r");
    int rozmiar;
    fscanf(grupy,"%d",&rozmiar);
    char nazwa[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupy,"%s",nazwa);
        if(strncmp(my_msg.napis1,nazwa,32)==0) {
            fclose(grupy);
            return 1;
        }
    }
    fclose(grupy);

    return 0;
}

void polecenie4(){
    my_msg.type = my_msg.ID;
    if(czyIstniejeDanaGrupa(my_msg.napis1)==0){
        my_msg.numerPolecenia = 0;
        strcpy(my_msg.napis3,"Nie istnieje taka grupa");
        return;
    }

    my_msg.numerPolecenia = 1;
    FILE *grupa;
    char pom[32] ;
    strcpy(pom, "./data/");
    strcat(pom, my_msg.napis1);
    strcat(pom, ".dat");
    
    grupa = fopen(pom,"r");
    if (grupa == NULL){
      printf("Error opening group file\n");
      return;
    }
    int rozmiar;
    fscanf(grupa,"%d",&rozmiar);
    my_msg.osoby.lacznie = rozmiar;
    int ID;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%d %s %s",&ID ,imie, nazwisko);
        strcpy(my_msg.osoby.dane[i].imie,imie);
        strcpy(my_msg.osoby.dane[i].nazwisko,nazwisko);
    }
    return;
}

//sprawdzam czy istnieje grupa, sprawdzam czy juz jest w tej grupie zapisujac do structa, jesli nie przepisuje structa dodajac noewgo czlonka
void polecenie5(){
    my_msg.type = my_msg.ID;
    if(czyIstniejeDanaGrupa(my_msg.napis1)==0){
        strcpy(my_msg.napis3,"Nie istnieje taka grupa");
        return;
    }

    struct pomoc bazaPomocnicza;
    FILE *grupa;
    char pom[32];
    strcpy(pom, "./data/");
    strcat(pom, my_msg.napis1);
    strcat(pom, ".dat");
    
    grupa = fopen(pom,"r");
    int rozmiar;
    fscanf(grupa,"%d",&rozmiar);
    bazaPomocnicza.lacznie = rozmiar;
    int ID;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%d %s %s",&ID ,imie, nazwisko);
        bazaPomocnicza.uzytkownicy[i].ID = ID;
        strcpy(bazaPomocnicza.uzytkownicy[i].imie,imie);
        strcpy(bazaPomocnicza.uzytkownicy[i].nazwisko,nazwisko);
        if(ID==my_msg.ID){
            strcpy(my_msg.napis3,"Znajdujesz sie juz w tej grupie");
            return;
        }
    }
    fclose(grupa);

    FILE *uzytkownicy;
    uzytkownicy = fopen("./data/users.dat","r");
    fscanf(uzytkownicy,"%d",&rozmiar);
    for(int i=0;i<rozmiar;i++){
        fscanf(uzytkownicy,"%d %s %s",&ID, imie, nazwisko);
        if(ID==my_msg.ID){
            fclose(uzytkownicy);
            FILE *plik2;
            plik2 = fopen(pom, "w");
            
            fprintf(plik2,"%d\n",bazaPomocnicza.lacznie+1);
            for(int i=0;i<bazaPomocnicza.lacznie;i++){
                fprintf(plik2,"%d %s %s\n",bazaPomocnicza.uzytkownicy[i].ID,
             bazaPomocnicza.uzytkownicy[i].imie, bazaPomocnicza.uzytkownicy[i].nazwisko);
            }
            fprintf(plik2,"%d %s %s",ID, imie, nazwisko);
            strcpy(my_msg.napis3,"Pomyslnie zapisano do grupy");
            
            fclose(plik2);
            return;
        }
    }
    return;
}
// to polecenie jest identyczne do poprzedniego, moze mozna wrzucic cos do funkcji by nie powielac kodu
void polecenie6(){
    my_msg.type = my_msg.ID;
    if(czyIstniejeDanaGrupa(my_msg.napis1)==0){
        strcpy(my_msg.napis3,"Nie istnieje taka grupa");
        return;
    }

    struct pomoc bazaPomocnicza;
    FILE *grupa;
    char pom[32];
    strcpy(pom, "./data/");
    strcat(pom, my_msg.napis1);
    strcat(pom, ".dat");
    
    grupa = fopen(pom,"r");
    int rozmiar;
    fscanf(grupa,"%d",&rozmiar);
    bazaPomocnicza.lacznie = rozmiar-1;
    int ID;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%d %s %s",&ID ,imie, nazwisko);
        if(ID!=my_msg.ID){
            bazaPomocnicza.uzytkownicy[i].ID = ID;
            strcpy(bazaPomocnicza.uzytkownicy[i].imie,imie);
            strcpy(bazaPomocnicza.uzytkownicy[i].nazwisko,nazwisko);
        }
    }
    fclose(grupa);

    FILE *plik2;
    plik2 = fopen(pom, "w");
            
    fprintf(plik2,"%d\n",bazaPomocnicza.lacznie);
    for(int i=0;i<bazaPomocnicza.lacznie;i++){
        fprintf(plik2,"%d %s %s\n",bazaPomocnicza.uzytkownicy[i].ID,
        bazaPomocnicza.uzytkownicy[i].imie, bazaPomocnicza.uzytkownicy[i].nazwisko);
        printf("%d %s %s\n",bazaPomocnicza.uzytkownicy[i].ID,
        bazaPomocnicza.uzytkownicy[i].imie, bazaPomocnicza.uzytkownicy[i].nazwisko);
    }
    fclose(plik2);
    strcpy(my_msg.napis3,"Pomyslnie usunieto z grupy");
    return;
}

void polecenie7(){
    my_msg.type = my_msg.ID;
    FILE *grupy;
    grupy = fopen("./data/grupyTematyczne.dat","r");
    int rozmiar;
    fscanf(grupy,"%d",&rozmiar);
    my_msg.osoby.lacznie = rozmiar;
    char nazwa[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupy,"%s",nazwa);
        strcpy(my_msg.osoby.dane[i].imie,nazwa);
    }
    fclose(grupy);
}

int czyIstniejeDanyUzytkownik(char *imieAdresata, char *nazwiskoAdresata){
    FILE *uzytkownicy;
    uzytkownicy = fopen("./data/users.dat","r");
    int rozmiar;
    fscanf(uzytkownicy,"%d",&rozmiar);
    int ID;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(uzytkownicy,"%d %s %s", &ID, imie, nazwisko);
        if(strncmp(imieAdresata,imie,32)==0&&strncmp(nazwiskoAdresata,nazwisko,32)==0) {
            fclose(uzytkownicy);
            return ID;
        }
    }
    fclose(uzytkownicy);

    return 0;
}

int czyUzytkownikJestZablokowany(int IDAdressata){
    FILE *blokowani;
    blokowani = fopen("./data/blocked.dat","r");
    int rozmiar;
    fscanf(blokowani,"%d",&rozmiar);
    int IDZablokowanego, IDBlokujacego;
    for(int i=0;i<rozmiar;i++){
        fscanf(blokowani,"%d %d", &IDBlokujacego, &IDZablokowanego);
        if(IDBlokujacego==IDAdressata&&IDZablokowanego==my_msg.ID){
            fclose(blokowani);
            return 1;
        }
        if(IDBlokujacego==my_msg.ID&&IDZablokowanego==IDAdressata){
            fclose(blokowani);
            return 1;
        }
    }
    fclose(blokowani);
    return 0;
}

int czyUzytkownikZablokowalGrupe(int IDAdressata,char *nazwaGrupy){
    FILE *blokowani;
    blokowani = fopen("./data/blockedGrup.dat","r");
    int rozmiar;
    fscanf(blokowani,"%d",&rozmiar);
    int IDBlokujacego;
    char nazwa[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(blokowani,"%d %s", &IDBlokujacego, nazwa);
        if(IDBlokujacego==IDAdressata&&strncmp(nazwa,nazwaGrupy,32)==0){
            fclose(blokowani);
            return 1;
        }
    }
    fclose(blokowani);
    return 0;
}

int czyUzytkownikJestZalogowany(int IDAdressata){
    for(int i=0;i<zalogowani.lacznie;i++)if(IDAdressata==zalogowani.uzytkownicy[i].ID) return 1;
    return 0;
}

void polecenie8(int mid){
    
    if(czyIstniejeDanaGrupa(my_msg.napis1)==0){
        strcpy(my_msg.napis3,"Nie istnieje taka grupa");
        return;
    }

    FILE *uzytkownicy;
    uzytkownicy = fopen("./data/users.dat","r");
    int rozmiar;
    fscanf(uzytkownicy,"%d",&rozmiar);
    int IDNadawcy;
    char imieNadawcy[64], nazwiskoNadawcy[32];
    
    for(int i=0;i<rozmiar;i++){
        fscanf(uzytkownicy,"%d %s %s",&IDNadawcy, imieNadawcy, nazwiskoNadawcy);
        //printf("ID: %d imie: %s nazwisko %s\n",ID, imie, nazwisko);
        if(IDNadawcy==my_msg.ID){
            break;
        }
    }

    strcat(imieNadawcy,nazwiskoNadawcy);
    strcpy(my_msg.napis2,imieNadawcy);

    FILE *grupa;
    char pom[32];
    strcpy(pom, "./data/");
    strcat(pom, my_msg.napis1);
    strcat(pom, ".dat");
    
    grupa = fopen(pom, "r");
    fscanf(grupa,"%d",&rozmiar);
    int ID;
    char imie[32], nazwisko[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%d %s %s",&ID ,imie, nazwisko);
        
        if(czyUzytkownikZablokowalGrupe(ID,my_msg.napis1)==1){
        printf("%s %s zablokowal grupe\n",imie, nazwisko);
        continue;
        }

        if(czyUzytkownikJestZalogowany(ID)==0){
        printf("%s %s zablokowal nadawca jest wylogowany\n",imie, nazwisko);
        continue;
        }

        my_msg.type = ID;
        my_msg.numerPolecenia = 10;
        // w napis1 jest nazwa grupy, w nazwa2 jest imie i nazwisko nadawcy, w nazwa3 jest wiadomosc
        printf("Oto wyslana wiadomosc:\n");
        printf("%s %s %s\n",my_msg.napis1, my_msg.napis2, my_msg.napis3); 
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
    }
    fclose(grupa);

    my_msg.type = my_msg.ID;
    my_msg.numerPolecenia = 8;
    strcpy(my_msg.napis3, "Wyslano wiadomosc do zalogowanych i nie zablokowanych uzytkonikow");
    msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);

    return;
}

void polecenie9(int mid){

    int IDAdressata = czyIstniejeDanyUzytkownik(my_msg.napis1,my_msg.napis2);
    if(IDAdressata == 0){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Nie istnieje taki uzytkownik");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }

    if(czyUzytkownikJestZablokowany(IDAdressata)==1){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Nie udalo sie wyslac wiadomosc");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }

    if(czyUzytkownikJestZalogowany(IDAdressata)==0){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Uzytkownik do ktorego probujesz sie dodzwonic, nie odpowiada. Prosimy zadzwonic pozniej.");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }

    FILE *uzytkownicy;
    uzytkownicy = fopen("./data/users.dat","r");
    int rozmiar;
    fscanf(uzytkownicy,"%d",&rozmiar);
    int IDNadawcy;
    char imieNadawcy[64], nazwiskoNadawcy[32];
    for(int i=0;i<rozmiar;i++){
        fscanf(uzytkownicy,"%d %s %s",&IDNadawcy, imieNadawcy, nazwiskoNadawcy);
        //printf("ID: %d imie: %s nazwisko %s\n",ID, imie, nazwisko);
        if(IDNadawcy==my_msg.ID){
            break;
        }
    }

    my_msg.type = IDAdressata;
    strcpy(my_msg.napis1,imieNadawcy);
    strcpy(my_msg.napis2,nazwiskoNadawcy);
    my_msg.numerPolecenia = 10;
    msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);

    my_msg.type = my_msg.ID;
    my_msg.numerPolecenia = 9;
    strcpy(my_msg.napis3,"Udalo sie wyslac wiadomosc");
    msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
    
    return;
}

void polecenie10(int mid){
    
    return;
}

void polecenie11(int mid){
    int IDAdressata = czyIstniejeDanyUzytkownik(my_msg.napis1,my_msg.napis2);
    if(IDAdressata == 0){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Nie istnieje taki uzytkownik");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }

    if(czyUzytkownikJestZablokowany(IDAdressata)==1){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Uzytkownik juz jest zablokowany");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }
    
    struct zablokowani bazaPomocnicza;
    FILE *grupa;
    grupa = fopen("./data/blocked.dat","r");
    int rozmiar;
    fscanf(grupa,"%d",&rozmiar);
    bazaPomocnicza.lacznie = rozmiar+1;
        
    for(int i=0;i<rozmiar;i++){
        fscanf(grupa,"%d %d",&bazaPomocnicza.blokowani[i].IDBlokujacego, 
        &bazaPomocnicza.blokowani[i].IDZablokowanego);
    }
    fclose(grupa);

    FILE *plik2;
    plik2 = fopen("./data/blocked.dat", "w");
            
    fprintf(plik2,"%d\n",bazaPomocnicza.lacznie);
    for(int i=0;i<rozmiar;i++){
        fprintf(plik2,"%d %d\n",bazaPomocnicza.blokowani[i].IDBlokujacego,
        bazaPomocnicza.blokowani[i].IDZablokowanego);
    }
    
    fprintf(plik2,"%d %d\n",my_msg.ID,IDAdressata);
    fclose(plik2);
    my_msg.type = my_msg.ID;
    strcpy(my_msg.napis3,"Zablokowano uzytkownika");
    msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
    return;
}

void polecenie12(int mid){
    if(czyIstniejeDanaGrupa() == 0){
        my_msg.type = my_msg.ID;
        strcpy(my_msg.napis3,"Nie istnieje taka grupa");
        msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
        return;
    }

    FILE *blokowani;
    blokowani = fopen("./data/blockedGrup.dat","r");
    int rozmiar;
    struct bazaZalogowanych grupaZablokowana;
    fscanf(blokowani,"%d",&rozmiar);
    grupaZablokowana.lacznie=rozmiar+1;
    for(int i=0;i<rozmiar;i++){
        fscanf(blokowani,"%d %s", &grupaZablokowana.uzytkownicy[i].ID, grupaZablokowana.uzytkownicy[i].imie);
        if(grupaZablokowana.uzytkownicy[i].ID==my_msg.ID&&strncmp(grupaZablokowana.uzytkownicy[i].imie,my_msg.napis1,32)==0){
            fclose(blokowani);
            my_msg.type = my_msg.ID;
            strcpy(my_msg.napis3,"Grupa jest juz zablokowana");
            msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
            return;
        }
    }
    fclose(blokowani);

    FILE *blokowani1;
    blokowani1 = fopen("./data/blockedGrup.dat","w");
    fprintf(blokowani1,"%d\n",grupaZablokowana.lacznie);
    for(int i=0;i<rozmiar;i++){
        fprintf(blokowani1,"%d %s\n", grupaZablokowana.uzytkownicy[i].ID,
         grupaZablokowana.uzytkownicy[i].imie);
    }
    fprintf(blokowani1,"%d %s", my_msg.ID,
        my_msg.napis1);
    fclose(blokowani1);

    my_msg.type = my_msg.ID;
    strcpy(my_msg.napis3,"Grupa zostala pomyslnie zablokowana");
    msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
}

int main(){
    //poczatek inicjalizacji
    int mid = msgget(0x123456, 0644|IPC_CREAT);
    logowania.lacznie = 0;
    zalogowani.lacznie = 0;
    //koniec inicjalizacji
    while(1==1){
      msgrcv(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),1,0);
      switch(my_msg.numerPolecenia){
            case 1:{
                polecenie1();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }
            case 2:{
                polecenie2();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }

            case 3:{
                polecenie3();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }

            case 4:{
                polecenie4();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }

            case 5:{
                polecenie5();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }

            case 6:{
                polecenie6();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }

            case 7:{
                polecenie7();
                msgsnd(mid, &my_msg, sizeof(my_msg)-sizeof(my_msg.type),0);
                break;
            }
            case 8:{
                polecenie8(mid);
                break;
            }
            case 9:{
                polecenie9(mid);
                break;
            }
            case 11:{
                polecenie11(mid);
                break;
            }
            case 12:{
                polecenie12(mid);
                break;
            }
            default:{
                printf("Giga error\n");
                break;
            }
        }
        
        wyswietlBazeLogowania();
        wyswietlBazeZalogowanych();

        
        
    }
    return 0;
}
