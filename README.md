# IPC_messenger
A simple implementation of inter process communication between multiple clients, using a centralized server process.

## Getting started

To produce necessary binaries please navigate to the root of the project and run `make compile`.

Two binaries will then be available to you:
- `server` - the central element of the implemented communication. Exchanges messages with clients
- `client` - associated with one client session. Needs an active server to operate

An example set of commands (starting in the project root):
```bash
make compile
./server &
./client
```

# Source files
- `src/server.c` - The server implementation. Receives messages, interprets them, exchanges data with database files and sends proper information back to the client.
- `src/client.c` - The implementation of a client. Provides a simple command line interface for sending commands to the server and displaying its replies.
