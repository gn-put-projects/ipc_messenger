MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

#-----------------------------------
# Path to source files
S=src

#---------------
# Compiler and compiler flags
CC=gcc
CFLAGS=-Wall -g

.PHONY: test compile clean

test: clean compile
	./server

compile: client server

server: $S/server.c
	$(CC) $(CFLAGS) $^ -o server

client: $S/client.c
	$(CC) $(CFLAGS) $^ -o client

clean:
	rm -f client server
